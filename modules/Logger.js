'use strict';
const fs = require('fs');
const defaultLogsDir = `${__dirname}/../logs`;

class Logger {
    static logError(msg, path = defaultLogsDir) {
        log(`${timeString()} [ERROR]: ${msg}`, path);
    }
    static logInfo(msg, path = defaultLogsDir) {
        log(`${timeString()} [INFO]: ${msg}`, path);
    }
    static logMsg(msg, path = defaultLogsDir) {
        log(`${timeString()} [MSG]: ${msg}`, path);
    }
}

function timeString() {
    let date = new Date();
    return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
}
function log(msg, path) {
    let date = new Date();
    fs.appendFile(`${path}/log_${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}.txt`,
        msg+"\n", 'utf8', ()=>{});
}

module.exports = Logger;
