'use strict';
const regExps = {
    email: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z_]+\.[a-zA-Z]{2,}$/,
    username: /^[A-Za-z0-9._]{5,25}$/,
    city: /^[a-zA-Z ]{2,}$/ //I leave it if we'll wish to use it one day
};
const fs = require('fs');
const request = require('request');
const secretKey = JSON.parse(fs.readFileSync("./config/captcha_Key.json",'utf-8')).secretKey;
const DbConnection = require('../DbConnection');
const Logger = require('../Logger');

class RegBackValidator {
    /**
     * Validates inputs.less. Returns promise. When resolved, it will returned an object
     * If validation passed:
     * { isValid: true, inputs.less: {} } // Values passed to
     * Otherwise:
     * { isValid: false, errors: {} } // Object with fields for each error with a short description
     * @param username
     * @param password
     * @param email
     * @param city
     * @param country
     * @param clientType
     * @param captcha
     * @returns {Promise<Object>}
     */
    validateAll({ username, password, email, country, clientType, captcha }) {
        return new Promise((resolve, reject) => {
            this.validation = {
                isValid: true,
                errors: {},
            };
            this.inputs = {
                username, password, email, country, clientType, captcha,
            };

            this.checkValidity()
                .then(
                    () => {
                        if (this.validation.isValid) resolve(this.inputs);
                        reject(this.validation);
                    },
                    (err) => {
                        reject(err);
                    });
        });
    }

    checkValidity() {
        return new Promise((resolve, reject) => {
            let promises = [];
            const fields = Object.keys(this.inputs);

            for(let input of fields) {
                promises.push(Validators[input](this.inputs[input]));
            }

            Promise.all(promises)
                .then((result) => {
                    for(let i = 0; i < fields.length; i++) {
                        if(result[i] !== true) {
                            this.validation.isValid = false;
                            this.validation.errors[fields[i]] = result[i];
                        }
                    }
                    resolve();
                }).catch((err) => {
                    Logger.logInfo(`{${__filename}: ${err}`);
                    reject(err);
                });
        });
    }

    /**
     * The returned promise, when resolved, will return true if value is
     * not used yet and false otherwise
     * @param field - Field to check (currently email or username)
     * @param data - Value of the field
     * @returns Promise<bool>
     */
    static checkExistence(field, data) {
        return Validators.fieldExists(field, data);
    }
}

let countriesList = [];
class Validators  {
    static email(email) {
        return new Promise((resolve, reject) => {
            if(!email.match(regExps.email)) resolve('invalid');
            Validators.fieldExists('email', email)
                .then((result) => resolve(result ? 'duplicate' : true))
                .catch((err) => {
                    reject(err);
                });
        });
    }
    static fieldExists(field, data) {
        return new DbConnection()
            .query(`SELECT * FROM users WHERE ${field}=$1`, [data])
            .then((result) => {
                result.conn.end();
                return result.rowsNumber > 0;
            }).catch((err) => {
                Logger.logInfo(`{${__filename}: ${err}`);
                throw err;
            });
    }

    static password(pw) {
        if(pw.length < 6) return 'short';
        if(pw.length > 64) return 'long';
        return true;
    }

    static username(username) {
        return new Promise((resolve) => {
            if (!username.match(regExps.username)) resolve('invalid');
            if (username.length < 3) resolve('short');
            if (username.length > 64) resolve('long');
            Validators.fieldExists('username', username)
                .then((result) => resolve(result ? 'duplicate' : true))
                .catch((err) => {
                    reject(err);
                });
        });
    }

    static country(country) {
        return Validators.isValidCountry(country)
            .then((result) => {
                return result ? true : 'unsupported';
            });
    }

    static clientType(clientType) {
        return clientType === "worker" || clientType === "client" ? true : 'invalid';
    }

    static captcha(response) {
        return new Promise((resolve, reject) => {
            const verificationUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${response}`;

            request.post(verificationUrl, (error, response, body) => {
                resolve(body);
            });
        }).then((body) => {
            const info = JSON.parse(body);
            return info.success ? true : 'failed';
        });
    }

    static isValidCountry(country) {
        return new Promise((resolve) => {
            if (Object.keys(countriesList).length === 0) {
                fs.readFile('config/availableCountries.json', 'utf8',
                    (err, data) => {
                        countriesList = JSON.parse(data);
                        resolve();
                    });
            } else resolve();
        }).then(() => {
            return countriesList.indexOf(country) !== -1;
        });
    }
}

module.exports = RegBackValidator;
