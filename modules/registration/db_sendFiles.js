'use strict';
const Connection = require("../DbConnection");
const uniqid = require('uniqid');
const bcrypt = require("bcrypt");

function passwordHash(plainPassword) {
    return new Promise((resolve) => {
        bcrypt.hash(plainPassword, 10, function (err, hash) {
            if (err) throw err;
            resolve(hash)
        });
    });
}

const sendData = (email, password, username, country, customer) => {
    return passwordHash(password).then(
        (hash) => {
            return new Connection()
                .query(
                    'INSERT INTO users (id, type, email, hash, username, country) ' +
                    'VALUES($1, $2, $3, $4, $5, $6)',
                    // Hubert: So when you wish it was uncommented? :P
                    // Alex: Right now ;P
                    [uniqid(), customer, email, hash, username, country]
                );
        },
        (err) => {
            // There is no reason to handle error here, so
            // we'll just log it and let it go higher :p
            require('../Logger').logError(err);
            throw err;
        }
    );
};

module.exports.sendData = sendData;
