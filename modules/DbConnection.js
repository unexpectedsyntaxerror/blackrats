'use strict';
const fs = require("fs");
const Client = require("pg-native");
const uniqid = require('uniqid');
const Logger = require('./Logger');

class DbConnection {
    constructor() {
        this.getClient = DbConnection.get_keys() // Get keys
            .then((keys) => {
                this.connectionString =
                    `postgresql://${keys.user}:${keys.password}@${keys.host}:${keys.port}/${keys.dbName}`;
                const client = new Client();
                return new Promise((resolve) => {
                    client.connect(this.connectionString, (err) => {
                        err ? reject(err) : resolve(client);
                    });
                });
            })
            .catch((err) => {
                Logger.logError(err);
                throw err;
            });
    }

    /**
     * Proceeds a query, quite obvious
     * @param query Query
     * @param data Data
     * @returns {Promise<Object>}
     */
    query(query, data) {
        return new Promise((resolve, reject) => {
            this.getClient.then((client) => {
                client.query(query, data, (err, rows) => {
                    const conn = this;
                    err ? reject(err) : resolve({
                        conn,
                        data: rows,
                        rowsNumber: rows.length
                    });
                });
            });
        }).catch((err) => {
            Logger.logError(err);
            throw err;
        });
    }

    /**
     * Example:
     * new DbConnection()
     *  .prepare('INSERT INTO users (username, email) VALUES ($1, $2)')
     *  .then((stmt) => {
     *      console.log(stmt); // Has 2 properties: stmtId with stmt id
     *                         // and execute method that executes statement
     *      stmt.execute(['username1', 'example1@gmail.com'])
     *          .then((result) => { // Result of query with username1
     *              console.log(result.rowsNumber); // Shows '1'
     *
     *              // Returning promise, so it's result will
     *              // be available in the next .then
     *              return stmt.execute(['username2', 'example2@gmail.com']);
     *          })
     *          .then((result) => { // Result of query with username2
     *              console.log(result.rowsNumber); // Also '1'
     *          });
     *  });
     * @param query Query
     * @param nParams Quantity of parameters, by default same as quantity of $'s in query
     * @returns {Promise<Object>}
     */
    prepare(query, nParams) {
        nParams = nParams || (query.match(/\$\d/g) || []).length;
        return new Promise((resolve, reject) => {
            this.getClient.then((client) => {
                const stmtId = uniqid();
                client.prepare(stmtId, query, nParams, (err) => {
                    const _this = this;
                    err ? reject(err) : resolve({
                        // Returns an object so that you could
                        // receive it in .then and use "execute" on it
                        id: stmtId,
                        execute: (values) => _this.execute(stmtId, values),
                    });
                });
            });
        }).catch((err) => {
            Logger.logError(err);
            throw err;
        });
    }

    /**
     * Executes prepared statement, quite obvious
     * @param stmt Prepared statement id
     * @param values Data
     * @returns {Promise<Object>}
     */
    execute(stmt, values = []) {
        return new Promise((resolve, reject) => {
            this.getClient.then((client) => {
                client.execute(stmt, values, (err, rows) => {
                    const conn = this;
                    err ? reject(err) : resolve({
                        conn,
                        data: rows,
                        rowsNumber: rows.length
                    });
                });
            });
        }).catch((err) => {
            Logger.logError(err);
            throw err;
        });
    }

    end() {
        this.getClient.then((client) => client.end());
    }

    static get_keys() {
        return new Promise((resolve) => {
            // Keys should be a static property so we
            // assign them to class not to object
            if(DbConnection.keys) resolve(DbConnection.keys);
            fs.readFile('config/db_keys.json', 'utf8', (err, data) => {
                if (err) throw err;
                resolve(JSON.parse(data));
            });
        });
    }
}

module.exports = DbConnection;
