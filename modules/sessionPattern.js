'use strict';
const Connection = require("./DbConnection.js");

class GetData {
  constructor(variant, session, firstDataObject, secondDataObject){

    this.session = session;
    return new Promise((resolve, reject) => {
      /** This object selects given objects from database or session asynchronously
        Session is ALWAYS checked in each of following variants:
        0 - no data required
        1 - only session data
        2 - only database data
        12 - both varaints together
        which are the very FIRST parameter

        SECOND parameter is session object (like req.session)

        THIRD and FORTH parameter are data objects
          dividing by variants:
            0 - doesn't need them
            (1 - session, 2 - database) object like:
                {
                tableName1 : [columnName1, columnName2...],
                tableName2 : [column1],
                ...};
            12 - reqires 2 objects such as above as 2 parameters:
                 first is database, second is session

          OUTPUT is thenable. Tested with 1 and 2 tables:
            On resolve: (when everything's OK)
              {
                isLogged : true  <- For easy-to-if at frontend
                data : {         <- Contains mixed, ready to render data from both session and database
                  table1: {         NOTE: If database's table.column is equal to session's one db data overwrites session data
                      column1 : value1,
                      column2 : value2
                      ...
                  }
                  table2:{
                      column1: value1,
                      ...
                  }
                  ...
                }
                session : {      <- That data is ready to merge with session object (false if data already exists in session)
                    (same structure as data's one)
                }
              }

            On reject: (while error)
              {
                isLogged : false,
                errorMsg : string <- contains short error message
              }

                Possible error messages:
                - noSession: If session is not set
                - queryError: Database error (probably query is wrong)
                - connectionError: Problem with connecting to database (rarer with generating query)
                - sessionError: Error occured while checking variables in session (rare error)
      **/

      if(this.session.userId){
        switch(variant){
          case 0:
            resolve();
            break;
          case 1:
            this.getSessionData(firstDataObject).then(
              (data) => {resolve(data)},
               (msg) => reject(msg));
            break;
          case 2:
            this.getDbData(firstDataObject).then(
              (data) => {resolve(data)},
               (msg) => reject(msg));
            break;
          case 12:
            let promises = [];
            promises.push(this.getDbData(firstDataObject));
            promises.push(this.getSessionData(secondDataObject));

            Promise.all(promises).then(
              (responses) => {
                if(!responses[1].session) responses[0]["session"] = this.objectParser(responses[1].data);
                  else responses[0]["session"] = false;
                let temp = Object.assign({}, responses[1].data, responses[0].data);
                responses[0].data = this.objectParser(temp);

                resolve(responses[0]);
            }, (err) => reject(err));
            break;
        }
      } else reject({
          isLogged : false,
          errorMsg : 'noSession'
        });
    });
  }

  getSessionData(tablesOfColumns){
    return new Promise((resolve, reject) => {
      this.checkSessionValues(tablesOfColumns).then(
        (checkResponse) => {
          if(checkResponse.status){
            resolve({ //If there's no unknown columns in session
              isLogged : true,
              session : true, //say that nothing requires to be put inside session (session contains all of data)
              data : checkResponse.objectResponse //and return data
          })}
          else //if there are some
            this.generateQueryAndConnect(tablesOfColumns).then(
              (data) => {  // get them from database
                resolve({
                  isLogged : true,
                  session : false, //say that data require to be put inside session
                  data : data.data[0]
                }) //data will be added to session inside controller, responseForUser is data for response
            }, (msg) => reject({
              isLogged : false,
              errorMsg : msg
            }))
        }, (msg) => reject({
          isLogged : false,
          errorMsg : msg
        })
      )
    });
  }

  getDbData(tablesOfColumns){
    return new Promise((resolve, reject) => {
      this.generateQueryAndConnect(tablesOfColumns).then(
        (data) => {resolve({
            isLogged : true,
            data : data.data[0]
          })}, (msg) => reject({
          isLogged : false,
          errorMsg : msg
        })
      );
    });
  }

  generateQueryAndConnect(dataObject){
    return new Promise((resolve, reject) => {
      try{
        let query = "SELECT";
        let tableCounter = 0;
        //SELECT table1.col1 AS table1.col1, lable1.col2 AS table1.col2, table2.col1 AS table2.col1...
        for(let table in dataObject){
          if(tableCounter) query+=","; //comma after each declared column
          let colCounter = 1;
          for (let column of dataObject[table]){
            if(colCounter!=1) query+=",";
            query+=` ${table}.${column} AS "${table}.${column}"`;
            colCounter++;
          }
          tableCounter++;
        }
        //FROM table1, table2, table3...
        tableCounter=1;
        query+=" FROM";
        for(let table in dataObject){
          if(tableCounter!=1) query+=",";
          query+=` ${table}`;
          tableCounter++
        }
        //WHERE table1.id='id' AND table2.id='id' AND...
        tableCounter=1;
        query+=" WHERE";
        for(let table in dataObject){
          if(tableCounter!=1) query+=" AND";
          query+=` ${table}.id=$1`;
          tableCounter++;
        }
        //Create connection with that query
        new Connection()
          .query(query, [this.session.userId]).then(
            (data) =>{
              resolve(data)
            },
            () => reject('queryError')
          );
      } catch(err){
        reject("connectionError");
      }
    });
  }

  checkSessionValues(tablesOfColumns){
    return new Promise((resolve, reject) => {
      try{
        let objectResponse = {};
        for(let table in tablesOfColumns){
          if(this.session[table] === undefined) return resolve({status : false});
          for(let column of tablesOfColumns[table]){
            if(this.session[table][column] === undefined) return resolve({status : false}); //If such column does not exist
            else objectResponse[`${table}.${column}`] = this.session[column]; //set property gotten from session
          }
        }
        resolve({
          status : true,
          objectResponse
        });
      } catch(err){
        console.log(err);
        reject('sessionError');
      }
    });
  }

  objectParser(objectParam){
    let newObject = {};

    for(let tableColumn in objectParam){
      let tableColumnArray = tableColumn.split(".");
      if(!newObject[tableColumnArray[0]]) newObject[tableColumnArray[0]] = {};
      newObject[tableColumnArray[0]][tableColumnArray[1]] = objectParam[tableColumn];
    }

    return newObject;
  }
}

module.exports.GetData = GetData;
