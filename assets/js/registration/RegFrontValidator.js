'use strict';
const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z_]+\.[a-zA-Z]{2,}$/;
const usernameRegex = /^[a-zA-Z0-9_]+$/;
const pwRegex = /^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+$/;
const tempTranslation = {
    duplicate: (field) => `Such ${field} already exists`,
    short: (field, limit) => `This ${field} is too short: it must be at least ${limit} characters long`,
    long: (field, limit) => `This ${field} is too long: it mustn'y be more than ${limit} characters long`,
    invalid: (field, message) => `${message}`,
    unsupported: (field) => `Such ${field} is not supported`,
    server: (error) => error === 'db_error' ?
        'Something went wrong when adding to data base. Try again later' :
        'There were some problems with server. Try again later',
    invalidEmail: 'This is not a valid e-mail',
    invalidUsername: 'Username can only contain latin letters, numbers and underscore (_)',
    invalidPw: 'Password can only contain latin letters, numbers and following symbols: ' +
        '!, #, $, %, &, \', *, +, /, =, ?, ^, _, `, {, |, }, ~ and -',
};

class RegFrontValidator {
    constructor() {
        const $form = /*this.$form =*/ $('#reg-form');
        this.fields = {
            email: $form.find('#reg__email'),
            password: $form.find('#reg__pw'),
            username: $form.find('#reg__username'),
            country: $form.find('#reg__country')
        };
        this.choices = {
            clientType: $form.find("#reg__client-type [type='radio']"),
        };
        this.errors = {};

        for(let field of Object.keys(this.fields)) {
            if (typeof this[field] === 'function') {
                this.fields[field]
                    .on('blur', () => {
                        this[field](this.fields[field].val().trim())
                            .then(() => { // Resolved
                                this.errors[field] = false;
                                this.fields[field]
                                    .closest('.input-row').attr('data-error', '');
                            }, ({reason, data = null}) => { // Rejected
                                this.errors[field] = true;
                                this.fields[field]
                                    .closest('.input-row').attr('data-error', tempTranslation[reason](field, data));
                            });
                    });

                // if (this.fields[field].val().trim() !== '')
                    // this.fields[field].trigger('blur');
                // else if (this.fields[field].data('optional') !== 'true')
                //     this.errors[field] = true;
            }
        }

        this.$loading = $('#js-loading-backdrop');
        if(this.$loading.length === 0) {
            this.$loading = $(`<div id="js-loading-backdrop"><i class="fa fa-spinner loading-spinner" /></div>`).appendTo('body');
        }

        $form.on('submit', (evt) => {
            evt.preventDefault();
            let hasError = false;
            for(let error of Object.keys(this.errors)) {
                if(this.errors[error]) {
                    hasError = true;
                    let field = this.fields[error].closest('.input-row');
                    field.addClass('highlight');
                    setTimeout(() => field.removeClass('highlight'), 300);
                }
            }
            if(hasError) return;

            setTimeout(() => this.$loading.addClass('visible'), 0);

            if(!grecaptcha) {
                alert("Something's wrong with reCaptcha! The page will be reloaded when you close this" +
                    " window. If it doesn't help, try again later");
                location.reload();
                return;
            }
            grecaptcha.ready(() => {
                grecaptcha.execute(
                    '6LcD7HwUAAAAAAmhUZ3lLM-vB8B1zd98zqtHfVQY',
                    {action: 'registration'}
                )
                    .then((token) => {
                        return this.proceedRegistration(token);
                    })
                    .then(
                        (result) => {
                            this.$loading.removeClass('visible');
                            $('.reg--block:not(.hidden)').addClass('passed');
                            $('.reg--block.hidden').removeClass('hidden');
                            setTimeout(() => {
                                $('.passed').removeClass('passed').addClass('hidden');
                            }, 1750);
                        },
                        (result) => {
                            console.log('rejected ', result);
                            $('.input-row[data-error]').attr('data-error', '');
                            if(result.status) {
                                alert(`An error happened during registration. Please, try again later\nError: ${result.status} - ${result.textError}`);
                            } else {
                                for (let field of Object.keys(result.errors)) {
                                    if (!this.fields[field]) continue;
                                    this.fields[field]
                                        .closest('.input-row').attr('data-error', tempTranslation[result.errors[field]](field));
                                }
                            }
                            this.$loading.removeClass('visible')
                        }
                    );
            });

            this.$loading.removeClass('visible');
        });

        $('.js-show-pw').on('click', function() {
            let $icon = $(this);
            let $input = $icon.prev('input');
            $icon.toggleClass('hidden');
            $input.attr('type',
                $input.attr('type') === 'password' ?
                    'text' : 'password');
        });
    }

    email(email) {
        return new Promise((resolve, reject) => {
            if(email === '')
                reject({reason: 'empty'});
            if(!email.match(emailRegex))
                reject({reason: 'invalid', data: tempTranslation.invalidEmail});
            $.ajax({
                method: 'GET',
                data: {
                    field: 'email',
                    data: email,
                },
                url: '/registration/exists',
                success(response) {
                    response ? resolve() : reject({reason: 'duplicate'});
                }
            });
        });
    }
    username(username) {
        return new Promise((resolve, reject) => {
            if(username === '')
                reject({reason: 'empty'});
            if(username.length < 4)
                reject({reason: 'short', data: 4});
            if(username.length > 64)
                reject({reason: 'long', data: 64});
            if(!username.match(usernameRegex))
                reject({reason: 'invalid', data: tempTranslation.invalidUsername});
            $.ajax({
                method: 'GET',
                data: {
                    field: 'username',
                    data: username,
                },
                url: '/registration/exists',
                success(response) {
                    response ? resolve() : reject({reason: 'duplicate'});
                }
            });
        });
    }
    password(password) {
        return new Promise((resolve, reject) => {
            if(password === '')
                reject({reason: 'empty'});
            if(password.length < 8)
                reject({reason: 'short', data: 8});
            if(password.length > 64)
                reject({reason: 'long', data: 64});
            if(!password.match(pwRegex))
                reject({reason: 'invalid', data: tempTranslation.invalidPw});
            resolve();
        });
    }

    proceedRegistration(captchaToken) {
        return new Promise((resolve, reject) => {
            let data = {captchaToken};
            for (let field of Object.keys(this.fields)) {
                data[field] = this.fields[field].val();
            }
            for (let choice of Object.keys(this.choices)) {
                data[choice] = this.choices[choice].filter(':checked').val();
            }
            console.log(data);
            $.ajax({
                url: "/registration",
                method: "POST",
                data: data,
                dataType: "json",
                success: (response) => {
                    response.isValid ? resolve() : reject(response);
                },
                error(xhr, error, textError) {
                    reject({
                        status: xhr.status,
                        textError
                    });
                },
            });
            // TODO: finish ajax
        });
    }
}
