'use strict';
function initHeaders() {
    const $headers = $('.js-sticky-header');
    const $window = $(window);
    const hiddenHeaders = [];
    const headerClones = [];
    const $headerContainer = $('<div id="js-sticky-header-container" />').prependTo('.content');

    $window.on('scroll', () => {
        for(let header of $headers) {
            let $header = $(header);

            let headerHidden = hiddenHeaders.indexOf(header) !== -1;
            let headerOffset = $header.offset().top - $window.scrollTop();
            if(!headerHidden && headerOffset <= 0) {
                /*
                 === Stick header ===
                 */
                hiddenHeaders.push(header); // Add header to hidden ones

                let $headerCopy = $header.clone(); // Create a copy
                $headerCopy.addClass('header--sticked'); // Add class for copy
                headerClones.forEach((clone) => clone.hide()); // Hide all previous copies
                headerClones.push($headerCopy); // Add copy to array of copies
                $headerContainer.append($headerCopy); // Add copy to dom

                $header.addClass('header--invisible'); // Hide header
            } else if (headerHidden && headerOffset > 0) {
                /*
                 === Release header ===
                 */
                headerClones.pop().remove(); // Remove header copy from array and dom
                // Show the last copy if exists
                let $lastClone = headerClones[headerClones.length - 1];
                if($lastClone) $lastClone.show();

                hiddenHeaders.pop(); // Remove header from hidden ones
                $header.removeClass('header--invisible'); // Show header
            }
        }
    });
}

initHeaders();