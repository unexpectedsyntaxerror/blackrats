'use strict';

/*
 *  Menu panel - panel that contains forms, settings, etc.
 *  Menu panel section - one part of panel: specific form, user panel, etc.
 */
function initMenuPanel() {
    const togglerActiveClass = 'main-menu__item--js-switch--menu-opened';
    const sectionVisibleClass = 'main-menu-panel__section--visible';

    const $menu = $('.main-menu');
    const $panel = $menu.find('.main-menu-panel');
    const $backdrop = $menu.find('.main-menu-panel__backdrop');
    const $panelSections = $panel.find('.main-menu-panel__section');
    const $headerTogglers = $menu.find('.main-menu__item--js-switch');
    let $currentSection = $();

    $panel.on({
        'show.menuPanel'() {
            $menu.addClass('shown');
            $backdrop.stop(true).fadeIn(400);
        },
        'hide.menuPanel'() {
            $menu.removeClass('shown');
            $backdrop.stop(true).fadeOut(400);
        },
    });
    $panel.isShown = () => $menu.hasClass('shown');

    function showSection(e) {
        e.preventDefault();

        const $toggler = $(e.currentTarget);

        // Get section id from href attribute
        let targetSectionId = $toggler.attr('href');
        // Section id must begin with '#' and contain only letters, numbers and dashes
        if(!targetSectionId.match(/#[\w-]+/))
            throw new Error(`${targetSectionId} is not a valid id for menu panel section`);
        // Remove leading '#'
        targetSectionId = targetSectionId.substr(1);

        // Find corresponding section
        const $targetSection = $panelSections.filter(`[data-section-id="${targetSectionId}"]`);
        if(!$targetSection.length)
            throw new Error(`Section with id #${targetSectionId} doesn't exist`);

        if($panel.isShown() && $currentSection[0] === $targetSection[0]) {
            $panel.trigger('hide.menuPanel');
            $toggler.removeClass(togglerActiveClass);
            return;
        }

        // If current shown panel is not the one we need
        // we have to switch thems
        if($currentSection[0] !== $targetSection[0]) {
            // Hide visible panels
            $panelSections
                .filter(`.${sectionVisibleClass}`)
                .removeClass(sectionVisibleClass);
            // Set new current
            $currentSection = $targetSection;
            // Make corresponding section visible
            $targetSection.addClass(sectionVisibleClass);
        }

        // Show panel
        $panel.trigger('show.menuPanel');
        // Change toggler color
        $toggler.addClass(togglerActiveClass);
    }

    function backdropHandle(e) {
        // We don't need clicks on content block
        if(e.target !== e.currentTarget) return;
        // Hide panel
        $panel.trigger('hide.menuPanel');
        $headerTogglers.filter(`.${togglerActiveClass}`).removeClass(togglerActiveClass);
    }

    $headerTogglers.on('click.menuPanel', showSection);
    $panel.on('click', backdropHandle);
}

initMenuPanel();
