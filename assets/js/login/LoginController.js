'use strict';

function isEmpty(object) {
    for (let i in object) if (object.hasOwnProperty(i)) return false;
    return true;
}

const tempEnglishResponses = {
    empty: (field) => `Please enter your ${field}`,
    wrongPwOrEmail: 'Wrong password or email. Please try again',
    tooManyUsers: 'You are multiplied in our database xD Please contact us!',
    dbProblems: 'Sorry an issue occured. Please, refresh site and try again!',
    captchaError: 'Sorry our captcha thinks you are a bot. Try to reopen your browser or restart internet connection',
    hashingProblems: 'Sorry we have some problems with your password, please try to login again'
};

class Login {
    constructor() {
        const $form = $("#login-form")

        $form.on("submit", (evt) => {
            evt.preventDefault();

            this.fields = {
                email: $form.find("#login__login").val(),
                password: $form.find("#login__pw").val()
            };
            let isEmpty = false;
            for (let field in this.fields) {
                if (this.fields[field].trim() === "") {
                    alert(tempEnglishResponses.empty(field));
                    isEmpty = true;
                }
            }
            if (isEmpty) return;
            if (!grecaptcha) {
                alert("Something's wrong with reCaptcha! The page will be reloaded when you close this" +
                    " window. If it doesn't help, try again later");
                location.reload();
                return;
            }
            grecaptcha.ready(() => {
                grecaptcha.execute('6LcD7HwUAAAAAAmhUZ3lLM-vB8B1zd98zqtHfVQY',
                    {action: 'login'}).then(
                    (token) => {
                        this.fields["captcha"] = token;

                        this.proceedLogin(this.fields).then(
                            () => {
                                //Everything is ok redirect to hub
                                location.href = "/cageHub";
                            },
                            (errors) => {
                                //Not OK, so switch among errors
                                $.each(errors, (field, type) => {
                                    switch (type) {
                                        case "empty":
                                            alert(tempEnglishResponses[type](field));
                                            break;
                                        default:
                                            alert(tempEnglishResponses[type]);
                                    }
                                });
                            }
                        );
                    }
                );
            });
        });
    }

    proceedLogin(data) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: "./login",
                method: "POST",
                data: data,
                dataType: "json",
                success: (response) => {
                    isEmpty(response.error) ? resolve() : reject(response.error);
                }
            });
        });
    }
}
