// nodemon -e .js,.less,.pug,.html,.css app.js
'use strict';
const bodyParser = require("body-parser");
const session = require("client-sessions");
const i18n = require('i18n');
const cookieParser = require('cookie-parser');

const express = require('express');
const app = express();

/*
Elements, I named as cageHub:
  - controllers/cageHubController.js
  - views/cageHub.pug
  - this file at lines 11 and 35
  - LoginController.js line 28
*/

app.set("view engine", "pug");
i18n.configure({
    defaultLocale: 'en',
    queryParameter: 'lang',
    directory: `${__dirname}/config/lang`,
    cookie: 'lang',
    updateFiles: false,
    autoReload: true,
    objectNotation: true,
    api: {
        __: 'tr',
    }
});
app.locals.view = {};
app.locals.view.menu = {
    logged: require('./config/view/menu-items-logged'),
    guest: require('./config/view/menu-items-guest'),
};

require('./controllers/dataLoadController')(app);

app
    .use('/assets', express.static('assets'))
    .use(bodyParser.urlencoded({extended: false}))
    .use(cookieParser())
    .use(i18n.init)
    .use((req, res, next) => {
        const logged = false; // TODO: add an actual check
        const menuItems = app.locals.view.menu[logged ? 'logged' : 'guest'](req.tr);
        const locale = req.getLocale();
        res.locals.renderOpts = {logged, menuItems, locale};
        next();
    })
    .use(session({
        cookieName: "session",
        secret: "$|Ou0#!Rf3?4*3@C32f#%$$#r32&!.9F$@#R)5s$#%Fd", //Some unbeatable session secret
        duration: 1000 * 3600,
        activeDuration: 1000 * 60 * 5 //when user move on our site, session extends always by 5min
    }));

require('fs').readdir('./controllers', (err, controllers) => {
    for(let controller of controllers)
        require(`./controllers/${controller}`)(app);
});

app.use((req, res, next) => { //mac middleware
  if(req.session.mac) return require('getmac').getMac(function(err, macAddress){
    if(err || req.session.mac != macAddress){
      req.session = {};
      console.log(req.session);
      res.redirect('/');
    } else next();
  });
  next();
});

app.listen(5000);

// reload(app);
