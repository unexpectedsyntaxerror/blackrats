'use strict';
module.exports = (app) => {
    app.locals.view = {
        menu: {
            logged: require('../config/view/menu-items-logged'),
            guest: require('../config/view/menu-items-guest'),
        },
    };
};