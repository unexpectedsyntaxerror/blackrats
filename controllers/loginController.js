'use strict';
const Connection = require("../modules/DbConnection");
const bcrypt = require("bcrypt");
const fs = require("fs");
const request = require("request");
const captchaSecret = JSON.parse(fs.readFileSync("./config/captcha_Key.json", 'utf-8')).secretKey;

module.exports = (app) => {

    app.post('/login', function (req, res) {
        class Login {
            constructor() {
                this.response = {
                    error: {}
                };
                this.fields = {
                    email: req.body.email,
                    password: req.body.password
                };
                let promises = [];

                promises.push(this.captchaCheck(req.body.captcha));
                promises.push(this.checkInputs());

                Promise.all(promises).then((response) => {
                    let isError = false;
                    for (let i = 0; i < 2; i++) if (!response[i]) isError = true;
                    if (!isError) {
                        return this.checkInDb().then((resp) => {
                            return this.checkPasswords(resp.password, resp.id);
                        });
                    }
                }).then(
                    () => res.send(this.response)
                );
            }

            checkInputs() {
                return new Promise((resolve, reject) => {
                    let i = true;
                    for (let field in this.fields) {
                        if (this.fields[field].trim() === "") {
                            this.response.error[field] = 'empty';
                            i = false;
                        }
                    }
                    resolve(i);
                });
            }

            checkInDb() {
                return new Promise((resolve, reject) => {
                    new Connection()
                        .query("SELECT * FROM users WHERE email=$1", [this.fields.email])
                        .then(
                            (resp) => {
                                switch (resp.rowsNumber) {
                                    case 0:
                                        this.response.error[''] = "wrongPwOrEmail";
                                        reject();
                                        break;
                                    case 1:
                                        resolve(resp.data[0]);
                                        break;
                                    default:
                                        this.response.error[''] = "tooManyUsers";
                                        reject();
                                }
                            },
                            () => {
                                this.response.error[''] = "dbProblems";
                                reject();
                            } //Checks if there was no db error
                        );
                });
            }

            checkPasswords(hashedPassword, userId) {
                return new Promise((resolve, reject) => {
                    //check if hashed and non-hashed passwords compare
                    bcrypt.compare(this.fields.password, hashedPassword, function (err, res) {
                        if (err) reject();
                        else resolve(res);
                    });
                }).then(
                    (res) => {
                        if(res){
                          return this.getMac().then((macResp) => {
                            req.session.userId = userId;
                            req.session.mac = macResp;
                          });
                        } else this.response.error[''] = "wrongPwOrEmail"; //If comompared set session userId else set error
                    },
                    () => {
                        this.response.error[''] = "hashingProblems";
                    }
                );
            }

            captchaCheck(token) {
                return new Promise((resolve, reject) => {
                    const verificationUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${captchaSecret}&response=${token}`;

                    request.post(verificationUrl, (error, response, body) => {
                        const info = JSON.parse(body);
                        if (!info.success) this.response.error['captcha'] = info['error-codes'];
                        resolve(info.success);
                    });
                });
            }

            getMac(){
              return new Promise((resolve, reject) => {
                require('getmac').getMac((err, address) => {
                  if(err) resolve(false);
                  else resolve(address);
                });
              });
            }
        }

        const loginBackController = new Login();
    });
};
