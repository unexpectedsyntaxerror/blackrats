'use strict';
const sessionClass = require("../modules/sessionPattern.js");

module.exports = (app) => {
  app.get('/cageHub', (req, res) => {
    let renderData = res.locals.renderOpts;
    let session = new sessionClass.GetData(1,
      req.session, {
      users : ["type", "email", "username", "country"]
    }).then(
      (data) => {
          Object.assign(renderData, data.data);
          if(data.session) Object.assign(req.session, data.session);
          res.render('cageHub.pug', renderData);
      },
      (error) => {
          Object.assign(renderData, error);
          res.redirect("/");
      }
    );
  });
};
