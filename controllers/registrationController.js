'use strict';
const database = require("../modules/registration/db_sendFiles");
const fs = require('fs');
const Logger = require('../modules/Logger');

module.exports = (app) => {
    app.route('/registration')
        .get((req, res) => {
            if (req.session.userId !== undefined) res.redirect('/'); //if user is not logged in redirect him back to the main page
            else fs.readFile('config/availableCountries.json', 'utf8', (err, data) => {
                const countries = JSON.parse(data);
                let responseData = {};
                Object.assign(responseData, res.locals.renderOpts, {countries});
                res.render("registration/registration.pug", responseData);
            });
        })
        .post((req, res) => {
            const regValidator = new (require('../modules/registration/RegBackValidator'))();
            regValidator.validateAll({
                email: req.body.email.trim(),
                username: req.body.username.trim(),
                password: req.body.password,
                country: req.body.country.trim(),
                clientType: req.body.clientType.trim(),
                captcha: req.body.captchaToken,
            }).then((inputs) => {
                database.sendData(inputs.email, inputs.password, inputs.username,
                    inputs.city, inputs.country, inputs.clientType === 'worker' ? 1 : 0)
                    .then(
                        (dbRes) => {
                            if (!dbRes) res.send({
                                isValid: false,
                                errors: {
                                    server: 'db_error',
                                },
                            });
                            else res.send({
                                isValid: true,
                            });
                        });
            }).catch((reason) => {
                if (reason.isValid !== undefined) res.send(reason);
                else {
                    Logger.logError(`{${__filename}: ${err}`);
                    res.sendStatus(500);
                }
            });
        });

    app.get('/registration/exists', function (req, res) {
        if ((req.query.field === 'email' || req.query.field === 'username') && typeof req.query.data === 'string') {
            const RegBackValidator = require('../modules/registration/RegBackValidator');
            RegBackValidator.checkExistence(req.query.field, req.query.data)
                .then((result) => res.send(result));
        } else res.sendStatus(404);
    });
};
