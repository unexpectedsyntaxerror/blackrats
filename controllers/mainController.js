'use strict';
const sessionClass = require("../modules/sessionPattern.js");

module.exports = (app) => {
    app.get('/', (req, res) => {
        let renderData = res.locals.renderOpts;
      let session = new sessionClass.GetData(1,
        req.session, {
        users : ["type", "username"]
      }).then(
            (data) => {
            Object.assign(renderData, data.data);
            if(data.session) Object.assign(req.session, data.session); //Merge session with responsed session object
                res.render('home.pug', renderData);
            },
            (error) => {
                Object.assign(renderData, error);
                res.render('home.pug', renderData);
            }
        );
    });

    app.get('/set_locale', (req, res) => {
        if(req.query && req.query.lang) res.cookie('lang', req.query.lang);
        res.redirect('/');
    });
};
