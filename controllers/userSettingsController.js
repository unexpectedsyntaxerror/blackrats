const sessionClass = require("../modules/sessionPattern.js");
module.exports = (app) => {
  app.get('/userSettings', (req, res) => {
      let renderData = res.locals.renderOpts;
      let session = new sessionClass(req.session.userId, "SELECT * FROM users WHERE id=$1").then(
        (data) => {
            Object.assign(renderData, data);
            res.render('userSettings.pug', renderData);
        },
        (error) => res.redirect('/')
      );
  });
};
