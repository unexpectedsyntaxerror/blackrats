module.exports = (tr) => [
    {
        active: true,
        link: '#dolor',
        icon: 'book-open',
        label: tr('lorem.dolor'),
    },
    {
        link: '#ipsum',
        icon: 'atom',
        label: tr('lorem.ipsum'),
    },
    {
        link: '#lorem',
        icon: 'balance-scale',
        label: tr('lorem._'),
    },
    {
        link: '#dolor',
        icon: 'book-open',
        label: tr('lorem.dolor'),
    },
    {
        link: '#ipsum',
        icon: 'atom',
        label: tr('lorem.ipsum'),
    }
];
