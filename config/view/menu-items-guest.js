module.exports = (tr) => [
    {
        active: true,
        link: '#lorem',
        icon: 'balance-scale',
        label: tr('lorem._'),
    },
    {
        link: '#ipsum',
        icon: 'atom',
        label: tr('lorem.ipsum'),
    },
    {
        link: '#dolor',
        icon: 'book-open',
        label: tr('lorem.dolor'),
    },
    {
        link: '/registration',
        icon: 'user-plus',
        label: tr('Register'),
    },
    {
        link: '#login',
        icon: 'sign-in-alt',
        label: tr('Log in'),
        isMenuSwitch: true,
    }
];
